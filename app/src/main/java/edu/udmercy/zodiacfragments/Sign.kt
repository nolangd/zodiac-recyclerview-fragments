package edu.udmercy.zodiacfragments

data class Sign(val id: Int,
                var name: String = "",
                var description: String = "",
                var month: String = "",
                var symbol: String = "")
