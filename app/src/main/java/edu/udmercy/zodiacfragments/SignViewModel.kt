package edu.udmercy.zodiacfragments

import androidx.lifecycle.ViewModel

class SignViewModel : ViewModel() {

    val signs = mutableListOf<Sign>()

    init {
        signs += Sign(0, "Aries", "Courageous and Energetic.", "April", "Ram")
        signs += Sign(1, "Taurus", "Known for being reliable, practical, ambitious and sensual", "May", "Bull")
        signs += Sign(2, "Gemini", "Gemini-born are clever and intellectual.", "June", "Twins")
        signs += Sign(3, "Cancer", "Tenacious, loyal and sympathetic.", "July", "Crab")
        signs += Sign(4, "Leo", "Warm, action-oriented and driven by the desire to be loved and admired.", "August", "Lion")
        signs += Sign(5, "Virgo", "Methodical, meticulous, analytical and mentally astute.", "September", "Virgin")
        signs += Sign(6, "Libra", "Librans are famous for maintaining balance and harmony.", "October", "Scales")
        signs += Sign(7, "Scorpio", "Strong willed and mysterious.", "November", "Scorpion")
        signs += Sign(8, "Sagittarius", "Born adventurers.", "December", "Archer")
        signs += Sign(9, "Capricorn", "The most determined sign in the Zodiac.", "January", "Goat")
        signs += Sign(10, "Aquarius", "Humanitarians to the core.", "February", "Water Bearer")
        signs += Sign(11,"Pisces","Proverbial dreamers of the Zodiac.","March","Fish")
    }

    fun getSigns(signId: Int): Sign {
        return signs.get(signId)
    }

}